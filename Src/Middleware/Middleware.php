<?php

namespace Ds\Middleware;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

/**
 * Middleware Base Class.
 *
 * Application middleware can either exend this class or implement MiddlewareInterface.
 *
 * @package Rs\Middleware
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link    https://github.com/djsmithme/middleware
 */
abstract class Middleware implements MiddlewareInterface
{
    /**
     * @var ContainerInterface
     */
    public $container;

    /**
     * @var LoggerInterface
     */
    public $logger;

    /**
     * Middleware constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->logger = new NullLogger();
    }

    /**
     * Return Logger
     *
     * @return LoggerInterface|NullLogger
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * With logger.
     *
     * @param LoggerInterface $logger PSR Logger
     *
     * @return Middleware
     */
    public function withLogger(LoggerInterface $logger)
    {
        $new = clone $this;
        $new->logger = $logger;
        return $new;
    }

    /**
     * Invoke Middleware
     *
     * @param RequestInterface $request Server Response
     * @param ResponseInterface $response Server Request
     * @param callable|null $next Next Middleware
     *
     * @return ResponseInterface
     */
    public function __invoke(
        RequestInterface $request,
        ResponseInterface $response,
        callable $next = null
    ) {
        if ($next) {
            $response = $next($request, $response);
        }
        return $response;
    }
}
