<?php

namespace Tests\Middleware;

use Ds\Middleware\Stack;

/**
 * Class StackTest
 * @package Tests\Middleware
 */
class StackTest extends \PHPUnit_Framework_TestCase
{

    public $request;
    public $response;
    public $stack;
    public $collector;
    public $container;

    public $objects = [];

    public function testGetNamespaces()
    {
        $expected = [];
        $actual = $this->stack->getNamespaces();
        $this->assertEquals($expected, $actual);
    }

    public function testWithNamespace()
    {
        $newStack = $this->stack->withNamespace('my-new-namespace');
        $expected = [
            'my-new-namespace'
        ];
        $actual = $newStack->getNamespaces();
        $this->assertEquals($expected, $actual);
    }

    /**
     * Private add method.
     */
    public function testAddCallable()
    {
    }

    public function testAddClass()
    {
    }

    public function testAddObject()
    {
    }

    /**
     * Public with method
     */
    public function testWithCallable()
    {
    }

    public function testWithClass()
    {
    }

    public function testWithObject()
    {
    }

    protected function setUp()
    {
        $this->container = $this->getMockBuilder('Interop\Container\ContainerInterface')->getMock();
        $this->request = $this->getMockBuilder('Psr\Http\Message\RequestInterface')->getMock();
        $this->response = $this->getMockBuilder('Psr\Http\Message\ResponseInterface')->getMock();
        $this->stack = new Stack();
    }
}
