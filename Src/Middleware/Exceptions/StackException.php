<?php

namespace Ds\Middleware\Exceptions;

/**
 * StackException
 *
 * @package Ds\Middleware\Exceptions
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link    https://github.com/djsmithme/middleware
 */
class StackException extends \Exception
{
}
