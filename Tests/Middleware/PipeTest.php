<?php

namespace Tests\Middleware;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Interop\Container\ContainerInterface;
use Ds\Middleware\Pipe;
use Ds\Middleware\PipeInterface;
use Ds\Middleware\StackInterface;


/**
 * Class PipeTest
 * @package Tests\Middleware
 */
class PipeTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|RequestInterface
     */
    public $request;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|ResponseInterface
     */
    public $response;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|StackInterface
     */
    public $stack;

    /**
     * @var \PHPUnit_Framework_MockObject_MockObject|ContainerInterface
     */
    public $container;

    /**
     * @var array
     */
    public $objects = [];

    /**
     * @var PipeInterface
     */
    public $pipe;

    /**
     *
     */
    public function testWithContainer()
    {
        $pipe = $this->pipe->withContainer($this->container);
        $this->assertSame($this->container, $pipe->getContainer());
    }

    /**
     *
     */
    public function testGetContainer()
    {
        $pipe = $this->pipe->withContainer($this->container);
        $this->assertSame($this->container, $pipe->getContainer());
    }

    /**
     *
     */
    public function testFromStack()
    {
        $name = 'stack-name';
        $routeName = ['foo'];

        $this->stack->expects($this->once())
                    ->method('getStack');

        $pipe = $this->pipe->fromStack($this->stack, $name, $routeName);

        $this->assertInstanceOf(PipeInterface::class, $pipe);
    }

    /**
     *
     */
    protected function setUp()
    {
        $this->request = $this->getMockBuilder(RequestInterface::class)->getMock();
        $this->response = $this->getMockBuilder(ResponseInterface::class)->getMock();
        $this->container = $this->getMockBuilder(ContainerInterface::class)->getMock();
        $this->stack = $this->getMockBuilder(StackInterface::class)->getMock();
        $this->pipe = new Pipe($this->container);
    }
}
