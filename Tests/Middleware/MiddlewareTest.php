<?php

namespace Tests\Middleware;

use Ds\Middleware\Middleware;
use Interop\Container\ContainerInterface;

/**
 * Class MockMiddleware
 * @package Tests\Middleware
 */
class MockMiddleware extends Middleware
{
    public function __construct(ContainerInterface $container = null)
    {
        //parent::__construct($container);
    }
}

/**
 * Class MiddlewareTest
 * @package Tests\Middleware
 */
class MiddlewareTest extends \PHPUnit_Framework_TestCase
{

    public $middleware;
    public $response;
    public $request;

    /**
     *
     */
    public function testInvoke()
    {
        $invoke = $this->middleware;
        $actual = $invoke($this->request, $this->response, null);
        $this->assertEquals($this->response, $actual);
    }

    /**
     *
     */
    protected function setUp()
    {
        $this->middleware = new MockMiddleware();
        $this->request = $this->getMockBuilder('Psr\Http\Message\RequestInterface')->getMock();
        $this->response = $this->getMockBuilder('Psr\Http\Message\ResponseInterface')->getMock();
    }
}
