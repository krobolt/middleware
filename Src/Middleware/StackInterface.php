<?php

namespace Ds\Middleware;

use Ds\Middleware\Exceptions\StackException;

/**
 * Interface: StackInterface.
 *
 * Stacks contain middleware collections.
 *
 * @package Ds\Middleware
 * @author  Dan Smith    <dan--smith@hotmail.com>
 * @license http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link    https://github.com/djsmithme/middleware
 */
interface StackInterface
{
    /**
     * Add Namespace to Stack.
     *
     * @param string $namespace Middleware namespace.
     *
     * @return StackInterface
     */
    public function withNamespace($namespace): StackInterface;

    /**
     * Get Stack namespaces.
     *
     * @return array
     */
    public function getNamespaces(): array;

    /**
     * Add Middleware to Collection.
     * Middleware can be either a class, string or callback.
     *
     * @param string|object|callable $middleware Classname, Class Object or Closure.
     * @param string $stack Stack name.
     * @param array $route Route Names.
     *
     * @return StackInterface
     * @throws StackException
     */
    public function withMiddleware($middleware, $stack = 'middleware', array $route = ['global']): StackInterface;

    /**
     * Return Middleware Stack.
     *
     * @param string $stackName Name of stack.
     * @param array $routeNames Route names.
     *
     * @return array
     */
    public function getStack($stackName, $routeNames = ['global']): array;
}
